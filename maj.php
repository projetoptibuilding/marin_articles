<!--
Cette page permet de comparer les dates de mises à jour des matériaux dans la table 'materaiux' et la table 'articles'
-->

<?php
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_GET['piece']);
    $id_scenario=htmlentities($_GET['scenario']);
    
    try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
    catch (Exception $e)
                {die('Erreur : ' . $e->getMessage());}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="#"/>
    </head>
    
    <body>
    <p><a href='../calcul/calcul_cout_global.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
    <input type='button' value='Retour au scénario'/></a></p>
    
    <p><a href='table_articles.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
    <input type='button' value='Table des articles'/></a></p>
    
    <p>Les articles suivants ont été mis à jour dans la base des matériaux. Cliquer sur Mettre à jour pour actualiser les caractéristiques de vos articles.</p>         
     <p>
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th>Code Matériau</th>
                    <th>MAJ</th>
                    <th>CUPI</th>
                    <th>Poste</th>
                    <th>Libellé</th>
                    <th>Fabriquant</th>
                    <th>Prix unitaire</th>
                    <th>Unité</th>
                    <th>Durée de vie</th>
                    <th>Taux entretien</th>
                    <th>Taux remplacement</th>
                </tr>
                </thead>
                     
                <tbody>
                                            
<?php   $affiche=$bdd->query('SELECT '.$projet.'.articles.*, optibuilding.materiaux.* FROM '.$projet.'.articles INNER JOIN optibuilding.materiaux ON '.$projet.'.articles.code_article = optibuilding.materiaux.code_mat
                             WHERE id_scenario='.$id_scenario.' AND '.$projet.'.articles.MAJ_article < optibuilding.materiaux.MAJ_mat ');
        
        
        while($donnes=$affiche->fetch()){
        ?>
                <tr>
                    <td>
                    <form method='post' action='mettre_a_jour.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
                        <input type='hidden' name='id_article' value='<?php echo $donnes['id_article'];?>'/>
                        <input type='hidden' name='CUPI_article' value='<?php echo $donnes['CUPI_mat'];?>'/>
                        <input type='hidden' name='MAJ_article' value='<?php echo $donnes['MAJ_mat'];?>'/>
                        <input type='hidden' name='libelle' value='<?php echo $donnes['libelle_mat']; ?>'/>
                        <input type='hidden' name='prix_unitaire' value='<?php echo $donnes['prix_unitaire_mat']; ?>' />
                        <input type='hidden' name='duree_de_vie' value='<?php echo $donnes['duree_de_vie_mat']; ?>'/>
                        <input type='hidden' name='taux_entretien' value='<?php echo $donnes['taux_entretien_mat']; ?>'/> 
                        <input type='hidden' name='taux_remplacement' value='<?php echo $donnes['taux_remplacement_mat']; ?>'/>
                        
                        <input type='submit' value='Mettre à jour' onclick="return confirm('Voulez-vous mettre à jour cet article ?');"/>
                    </form>
<!-- On atteind la page de modification de matériau en faisant passer l'id du matériau par un POST -->                   
                               
                    </td>
                    <td><?php echo $donnes['code_article']; ?></td>
                    <td><?php echo $donnes['MAJ_article'];?></td>
                    <td><?php echo $donnes['CUPI_article'];?></td>
                    <td><?php echo $donnes['poste']; ?></td>
                    <td><?php echo $donnes['libelle']; ?></td>
                    <td><?php echo $donnes['fabricant']; ?></td>
                    <td><?php echo $donnes['prix_unitaire']; ?></td>
                    <td><?php echo $donnes['unite']; ?></td>
                    <td><?php echo $donnes['duree_de_vie']; ?></td>
                    <td><?php echo $donnes['taux_entretien']; ?></td>
                    <td><?php echo $donnes['taux_remplacement']; ?></td>
                </tr>

                <tr>
                    <td></td>
                    <td><?php echo $donnes['code_mat']; ?></td>
                    <td><?php echo $donnes['MAJ_mat'];?></td>
                    <td><?php echo $donnes['CUPI_mat'];?></td>
                    <td><?php echo $donnes['poste_mat']; ?></td>
                    <td><?php echo $donnes['libelle_mat']; ?></td>
                    <td><?php echo $donnes['fabricant_mat']; ?></td>
                    <td><?php echo $donnes['prix_unitaire_mat']; ?></td>
                    <td><?php echo $donnes['unite_mat']; ?></td>
                    <td><?php echo $donnes['duree_de_vie_mat']; ?></td>
                    <td><?php echo $donnes['taux_entretien_mat']; ?></td>
                    <td><?php echo $donnes['taux_remplacement_mat']; ?></td>
                </tr>
<?php } ?>
                </tbody>
        </table></p>

    </body>
</html>
