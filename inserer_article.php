<!--
Cette page récupère les informations relatives à l'article que l'on veut insérer dans la table article de notre projet.
Ces informations proviennent de la page d'importation de matériaux ou du formulaire de création d'un nouvel article.
Les caractéristiques de l'article sont reçues par la méthode POST
-->
<?php
       $id_projet=htmlentities($_GET['projet']);
       $projet="projet_".$id_projet;
       $id_piece=htmlentities($_GET['piece']);
       $id_scenario=htmlentities($_GET['scenario']);
        
        try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                                           array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
        
        if (isset($_POST['surface'])) {  
       $req=$bdd->prepare('INSERT INTO articles(id_scenario, id_piece, code_article, poste, CUPI_article, MAJ_article, type_materiau,
                          libelle, fabricant, surface, unite, prix_unitaire, duree_de_vie, taux_entretien, taux_remplacement)
                            VALUES(:id_scenario, :id_piece, :code_article, :poste, :CUPI_article, :MAJ_article, :type_materiau,
                            :libelle, :fabricant, :surface, :unite, :prix_unitaire, :duree_de_vie, :taux_entretien, :taux_remplacement)');
       
       $req->execute(array('id_scenario'=>$id_scenario, 'id_piece'=>$id_piece, 'code_article'=>htmlentities($_POST['code_mat']),
                            'CUPI_article'=>htmlentities($_POST['CUPI_article']), 'MAJ_article'=>htmlentities($_POST['MAJ_article']),
                            'poste'=>htmlentities($_POST['poste']), 'type_materiau'=>htmlentities($_POST['type_materiau']),
                            'libelle'=>htmlentities($_POST['libelle']), 'fabricant'=>htmlentities($_POST['fabricant']),
                            'surface'=>htmlentities($_POST['surface']), 'unite'=>htmlentities($_POST['unite']),
                            'prix_unitaire'=>htmlentities($_POST['prix_unitaire']), 'duree_de_vie'=>htmlentities($_POST['duree_de_vie']),
                            'taux_entretien'=>htmlentities($_POST['taux_entretien']),
                            'taux_remplacement'=>htmlentities($_POST['taux_remplacement'])
                            ));
        }else{
       $req=$bdd->prepare('INSERT INTO articles(id_scenario, id_piece, code_article, poste, CUPI_article, MAJ_article, type_materiau,
                           libelle, fabricant, unite, prix_unitaire, duree_de_vie, taux_entretien, taux_remplacement)
                            VALUES(:id_scenario, :id_piece, :code_article, :poste, :CUPI_article, :MAJ_article, :type_materiau,
                            :libelle, :fabricant, :unite, :prix_unitaire, :duree_de_vie, :taux_entretien, :taux_remplacement)');
       
       $req->execute(array('id_scenario'=>$id_scenario, 'id_piece'=>$id_piece, 'code_article'=>htmlentities($_POST['code_mat']),
                            'CUPI_article'=>htmlentities($_POST['CUPI_article']), 'MAJ_article'=>htmlentities($_POST['MAJ_article']),
                            'poste'=>htmlentities($_POST['poste']), 'type_materiau'=>htmlentities($_POST['type_materiau']),
                            'libelle'=>htmlentities($_POST['libelle']), 'fabricant'=>htmlentities($_POST['fabricant']),
                            'unite'=>htmlentities($_POST['unite']), 'prix_unitaire'=>htmlentities($_POST['prix_unitaire']),
                            'duree_de_vie'=>htmlentities($_POST['duree_de_vie']),
                            'taux_entretien'=>htmlentities($_POST['taux_entretien']),
                            'taux_remplacement'=>htmlentities($_POST['taux_remplacement'])
                            ));        
        }
        
        header('Location:table_articles.php?projet='.$id_projet.'&piece='.$id_piece.'&scenario='.$id_scenario.'');
?>