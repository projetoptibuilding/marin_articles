<!--
Cette page comporte un formulaire qui permet d'insérer un nouvel article dans la table 'articles'
-->

<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="#"/>
    </head>
    
    <body>
     
    <header>
    </header>
                
    <section>
        
<?php   $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);
        $id_scenario=htmlentities($_GET['scenario']);
?>
        <h1>Créer un nouvel article, qui n'existe pas dans la base de données</h1>
            
        <form method='post' action='inserer_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
        <p>
        <fieldset>
            <legend>Nouvel article</legend>
            
            <label for='code_mat'>Code du matériau</label>
            <input type='text' id='code_mat' name='code_mat'/></br></br>
            
            <label for='poste'>Poste</label>
            <input type='text' id='poste' name='poste'/></br></br>
            
            <label for='type_materiau'>Type du matériau</label>
            <input type='text' id='type_materiau' name='type_materiau'/></br></br>
            
            <label for='libelle'>Libellé</label>
            <textarea name='libelle' rows="4" cols="45" placeholder='Libellé' ></textarea>
            
            <label for='fabricant'>Fabricant</label>
            <input type='text' id='fabricant' name='fabricant'/></br></br>
            
            <label for='surface'>Quantité</label>
            <input type='number' id='surface' name='surface' min="0" step="0.01"/></br></br>
            
            <label for='prix_unitaire'>Prix unitaire<em>(Utiliser des points et non pas des virgules)</em></label>
            <input type='numbre' id='prix_unitaire' name='prix_unitaire'/>
            
            <select name="unite" id="unite">
                <option value="m">m</option>
                <option value="m2" selected="selected">m2</option>
                <option value="unitaire">unitaire</option>
            </select></br></br>
            
            <label for='duree_de_vie'>Durée de vie <em>(en années)</em></label>
            <input type='number' id='duree_de_vie' name='duree_de_vie'/></br></br>
            
            <label for='taux_entretien'>Taux d'entretien annuel <em>(en pourcentage)</em></label>
            <input type='number' id='taux_entretien' name='taux_entretien' value="0.03" min="0" step="0.0001"/></br></br>
            
            <label for='taux_remplacement'>Taux de remplacement <em>(en pourcentage)</em></label>
            <input type='number' id='taux_remplacement' name='taux_remplacement' value="1" min="0" step="0.0001"/></br></br>
            
            <input type='submit' value='Créer'/>
            <input type='reset' value='Reset'/>
            </br></br>
                
        </fieldset>
        </p>
        </form>
            
        <p><a href='table_articles.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
        <input type='button' value='Annuler'/></a></p>
        
        <p><a href='../calcul/calcul_cout_global.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
        <input type='button' value='Retour au scénario'/></a></p>
 
     </section>
                             
    <footer>
    </footer>
                             
    </body>
</html>
