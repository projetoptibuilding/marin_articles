<!--
Cette page affiche un formulaire de modification d'un article de la table 'articles'.
On récupère l'id du matériau via l'URL puis on affiche un formulaire préremplit avec les infos sur l'article
-->

<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="#"/>
    </head>
        
    <body>
        <section>
        <p>Cette page vous permet de modifier un article dans la table.</p>
        
<?php   $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);
        $id_scenario=htmlentities($_GET['scenario']);
        $id=$_POST['id_article'];
?>
            <form method='post' action='modifier_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
                <p>
                <fieldset>
<?php        
        try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
        
        
        $affiche=$bdd->query('SELECT* FROM articles WHERE id_article='.$id.'');
        $mat=$affiche->fetch()
?>                    
                <legend>Pour modifier un matériau</legend>
                
                <input type="hidden" name="id_article" value="<?php echo $id; ?>"/>
                <input type="hidden" name="MAJ_article" value="<?php echo $mat['MAJ_article']; ?>"/>
                
                <label for='code_article'>Code du matériau</label>
                <input type='text' id='code_article' name='code_article' value='<?php echo $mat['code_article']; ?>'/></br></br>
                
                <label for='CUPI'>CUPI (mettre O si oui)</label>
                <input type='text' id='CUPI' name='CUPI_article' value'<?php echo $mat['CUPI_article'];?>'/></br></br>
                
                <label for='type_materiau'>Type du matériau</label>
                <input type='text' id='type_materiau' name='type_materiau' value='<?php echo $mat['type_materiau']; ?>'/></br></br>
                
                <label for='libelle'>Libellé</label>
                <textarea name='libelle' rows="4" cols="45"><?php echo $mat['libelle']; ?></textarea>
                
                <label for='fabricant'>Fabricant</label>
                <input type='text' id='fabricant' name='fabricant' value='<?php echo $mat['fabricant']; ?>'/></br></br>
                
                <label for='prix_unitaire'>Quantité</label>
                <input type='number' id='surface' name='surface' value='<?php echo $mat['surface']; ?>' min="0" step="0.001" style="width:50px;"/>
                
                <select name="unite" id="unite">
                <option value="m">m</option>
                <option value="m2" selected="selected">m2</option>
                <option value="unitaire">unitaire</option>
                </select></br></br>
                
                <label for='prix_unitaire'>Prix unitaire<em>(Utiliser des points et non pas des virgules)</em></label>
                <input type='number' id='prix_unitaire' name='prix_unitaire' value='<?php echo $mat['prix_unitaire']; ?>' min="0" step="0.01" style="width:60px;"/></br></br>
                
                <label for='duree_de_vie'>Durée de vie </label>
                <input type='text' id='duree_de_vie' name='duree_de_vie' value='<?php echo $mat['duree_de_vie']; ?>' style="width:50px;"/> années</br></br>
                
                <label for='taux_entretien'>Taux d'entretien annuel </label>
                <input type='number' id='taux_entretien' name='taux_entretien' value="<?php echo $mat['taux_entretien']; ?>" min="0" step="0.0001" style="width:50px;"/> %</br></br>
                
                <label for='taux_remplacement'>Taux de remplacement </label>
                <input type='number' id='taux_remplacement' name='taux_remplacement' value="<?php echo $mat['taux_remplacement']; ?>" min="0" step="0.0001" style="width:50px;"/> %</br></br>
                
                <input type='submit' value='Modifier'/>
                <input type='reset' value='Reset'/>
                </br></br>
                        
                    </fieldset>
                </p>
                </form>
                
                <p><a href='table_articles.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
                <input type='button' value='Annuler'/></a></p>
                 
                <p><a href='../calcul/calcul_cout_global.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
                <input type='button' value='Retour au scénario'/></a></p>
        </section>
                         
        <footer>         
        </footer>

    </body>
</html>
