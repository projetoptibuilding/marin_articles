<!--
Cette page affiche tous les articles ayant été sélectionnés pour le scénario étudié
-->

<!DOCTYPE html>
<html>
<head>
    <title>OptiBuilding</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="#"/>
</head>
        
<body>
<?php
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_GET['piece']);
    $id_scenario=htmlentities($_GET['scenario']);
?>
    
     <p><a href='../calcul/calcul_cout_global.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
     <input type='button' value='Retour au scenario'/></a></p>
     <p><table>
        <thead>
                <tr>
                    <th></th>
                    <th>Code Matériau</th>
                    <th>MAJ</th>
                    <th>CUPI</th>
                    <th>Poste</th>
                    <th>Type</th>
                    <th>Libellé</th>
                    <th>Fabricant</th>
                    <th>Quantité</th>
                    <th>Prix unitaire</th>
                    <th>Unité</th>
                    <th>Durée de vie</th>
                    <th>Taux entretien</th>
                    <th>Taux remplacement</th>
                </tr>
            </thead>
        <tbody>
        <?php 
        try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
                    
        $affiche=$bdd->query('SELECT* FROM articles WHERE id_piece="'.$id_piece.'" AND id_scenario="'.$id_scenario.'" ORDER BY code_article'); //filtrer par pièces et scénarios
        
        while($donnes=$affiche->fetch())
        {?>
        <tr>
            <td>
                 <form method='post' action='modification_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
                        <input type='hidden' name='id_article' value='<?php echo $donnes['id_article']; ?>'/>
                        <input type='submit' value='Modifier'/>
                    </form>
<!-- On atteind la page de modification de matériau en faisant passer l'id du matériau par un POST -->                   
                     <form method='post' action='retirer_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
                        <input type='hidden' name='id_article' value='<?php echo $donnes['id_article']; ?>'/>
                        <input type='submit' value='Retirer' onclick="return confirm('Voulez-vous vraiment retirer cet article ?');"/>
                    </form>
<!-- On atteind le code de suppression de l'entrée choisie, au moment du clic, une page d'avertissement d'affiche demande de confirmer l'action -->
            </td>
            <td><?php echo $donnes['code_article']; ?></td>
            <td><?php echo $donnes['MAJ_article'];?></td>
            <td><?php echo $donnes['CUPI_article'];?></td>
            <td><?php echo $donnes['poste']; ?></td>
            <td><?php echo $donnes['type_materiau']; ?></td>
            <td><?php echo $donnes['libelle']; ?></td>
            <td><?php echo $donnes['fabricant']; ?></td>
            <td><?php echo $donnes['surface']; ?></td>
            <td><?php echo $donnes['prix_unitaire']; ?></td>
            <td><?php echo $donnes['unite']; ?></td>          
            <td><?php echo $donnes['duree_de_vie']; ?></td>
            <td><?php echo $donnes['taux_entretien']; ?></td>
            <td><?php echo $donnes['taux_remplacement']; ?></td>
        </tr>       
        <?php } ?>
        </tbody> 
     </table></p>
     
     <p><a href='importer_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
     <input type='button' value='Importer un article de la base de données'/></a></p>
     
    <p><a href='nouvel_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
    <input type='button' value='Insérer un nouvel article'/></a></p>
    
    <p><a href='maj.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
    <input type='button' value='Vérifier les mises à jour'/></a></p>
</body>
</html>