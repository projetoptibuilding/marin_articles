<?php
        $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);
        $id_scenario=htmlentities($_GET['scenario']);
        $mat=htmlentities($_GET['mat']);

        try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
            {die('Erreur : ' . $e->getMessage());}
        
        
        $affiche=$bdd->query('SELECT* FROM articles WHERE code_article="'.$mat.'" AND id_piece='.$id_piece.' AND id_scenario='.$id_scenario.'');
        $article=$affiche->fetch()
?>

<!DOCTYPE html>
<html>
    <head>
        <title>OptiBuilding</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="#"/>
    </head>
        
    <body>
        <section>
        <p>Fiche matériau <?php echo $article['code_article'];?></p>

                
                CUPI : <?php echo $article['CUPI_article'];?></br></br>
                
                Type du matériau : <?php echo $article['type_materiau']; ?></br></br>
                
                Libellé : <?php echo $article['libelle']; ?></br></br>
                
                Fabricant :<?php echo $article['fabricant']; ?></br></br>
                
                Quantité :<?php echo $article['surface'].' '.$article['unite'];?></br></br>
                           
                Prix unitaire : <?php echo $article['prix_unitaire'];?> €</br></br>
                
                Durée de vie : <?php echo $article['duree_de_vie']; ?> années</br></br>
                
                Taux d'entretien annuel : <?php echo $article['taux_entretien']; ?></br></br>
                
                Taux de remplacement : <?php echo $article['taux_remplacement']; ?></br></br>
        </p>
        
        <p><form method='post' action='../articles/modification_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
        <input type='hidden' name='id_article' value='<?php echo $article['id_article'];?>'/>
        <input type='submit' value='Modifier' onclick="return confirm('Attention vous allez quitter l analyse du calcul.\nOK pour continuer\nAnnuler pour rester sur la page')"/></p>
        
        
        <p><a href='../calcul/analyse.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
        <input type='button' value='Retour'/></a></p>
        
        </section>

    </body>
</html>
