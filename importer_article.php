<!--
Cette page affiche les matériaux disponibles dans la table 'materiaux', on choisit le matériau que l'on veut insérer dans la table 'article'
relative au projet en cours.
Affichage sous forme de tableau, chaque ligne présente un bouton pour limportation d'un matériau sélectionné
-->

<!DOCTYPE html>
<html>
<head>
    <title>OptiBuilding</title>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="#"/>
</head>
        
<body>
    
<?php
    $id_projet=htmlentities($_GET['projet']);
    $projet="projet_".$id_projet;
    $id_piece=htmlentities($_GET['piece']);
    $id_scenario=htmlentities($_GET['scenario']);
?>
    <p><a href='../calcul/calcul_cout_global.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
    <input type='button' value='Retour au scénario'/></a></p>
    
    <p><a href='table_articles.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
    <input type='button' value='Table des articles'/></a></p>
    
    <p>Sélectionner les articles à importer</p>
    
    <p><table>
        <thead>
                <tr>
                    <th></th>
                    <th>Code Matériau</th>
                    <th>MAJ</th>
                    <th>CUPI</th>
                    <th>Poste</th>
                    <th>Type Matériau</th>
                    <th>Libellé</th>
                    <th>Fabricant</th>
                    <th>Prix unitaire</th>
                    <th>Unité</th>
                    <th>Durée de vie</th>
                    <th>Taux d'entretien annuel</th>
                    <th>Taux de remplacement</th>
                </tr>
            </thead>
        <tbody>
        <?php 
        try {$bdd= new PDO ('mysql:host=localhost;dbname=optibuilding;charset=utf8', 'root', '',
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}

        $affiche=$bdd->query('SELECT* FROM materiaux ORDER BY code_mat');
        while($donnes=$affiche->fetch())
        {?>
        <tr>
            <td>
                <form method='post' action='inserer_article.php?projet=<?php echo $id_projet;?>&piece=<?php echo $id_piece;?>&scenario=<?php echo $id_scenario;?>'>
                    <input type='submit' value='Importer'/>
<!-- Attention, les noms des input ne conmporte pas le suffixe '_mat', en effet on envoit les infos sur la page inserer_article.php, celle-ci triate des infos
provenant de plusieurs pages, on doit harmoniser les noms -->
                    <input type='hidden' name='MAJ_article' value='<?php echo $donnes['MAJ_mat'];?>'/>
                    <input type='hidden' name='CUPI_article' value='<?php echo $donnes['CUPI_mat'];?>'/>
                    <input type='hidden' name='code_mat' value='<?php echo $donnes['code_mat']; ?>'/>
                    <input type='hidden' name='poste' value='<?php echo $donnes['poste_mat']; ?>'/>
                    <input type='hidden' name='type_materiau' value='<?php echo $donnes['type_mat']; ?>'/>
                    <input type='hidden' name='libelle' value='<?php echo $donnes['libelle_mat']; ?>'/>
                    <input type='hidden' name='fabricant' value='<?php echo $donnes['fabricant_mat']; ?>'/>
                    <input type='hidden' name='prix_unitaire' value='<?php echo $donnes['prix_unitaire_mat']; ?>'/>
                    <input type='hidden' name='unite' value='<?php echo $donnes['unite_mat']; ?>'/>
                    <input type='hidden' name='duree_de_vie' value='<?php echo $donnes['duree_de_vie_mat']; ?>'/>
                    <input type='hidden' name='taux_entretien' value='<?php echo $donnes['taux_entretien_mat']; ?>'/>
                    <input type='hidden' name='taux_remplacement' value='<?php echo $donnes['taux_remplacement_mat']; ?>'/>
                </form>    
            </td>
            <td><?php echo $donnes['code_mat']; ?></td>
            <td><?php echo $donnes['MAJ_mat'];?></td>
            <td><?php echo $donnes['CUPI_mat'];?></td>
            <td><?php echo $donnes['poste_mat']; ?></td>
            <td><?php echo $donnes['type_mat']; ?></td>
            <td><?php echo $donnes['libelle_mat']; ?></td>
            <td><?php echo $donnes['fabricant_mat']; ?></td>
            <td><?php echo $donnes['prix_unitaire_mat']; ?></td>
            <td><?php echo $donnes['unite_mat']; ?></td>          
            <td><?php echo $donnes['duree_de_vie_mat']; ?></td>
            <td><?php echo $donnes['taux_entretien_mat']; ?></td>
            <td><?php echo $donnes['taux_remplacement_mat']; ?></td>
        </tr>       
        <?php } ?>               
        </tbody> 
     </table></p>
</body>
</html>