<!--
Cette page exécute la modification d'un matériau dans la table 'articles'. Les informations sont reçues par la méthode POST
-->

<?php
        $id_projet=htmlentities($_GET['projet']);
        $projet="projet_".$id_projet;
        $id_piece=htmlentities($_GET['piece']);
        $id_scenario=htmlentities($_GET['scenario']);
        $id_article=htmlentities($_POST['id_article']);

        echo htmlentities($_POST['MAJ_article']);
        echo htmlentities($_POST['CUPI_article']);
        echo htmlentities($_POST['libelle']);
        echo htmlentities($_POST['prix_unitaire']);
        echo htmlentities($_POST['duree_de_vie']);
        echo htmlentities($_POST['taux_entretien']);
        echo htmlentities($_POST['taux_remplacement']);
        
        
        try {$bdd= new PDO ('mysql:host=localhost;dbname='.$projet.';charset=utf8', 'root', '',
                               array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));}
        catch (Exception $e)
                    {die('Erreur : ' . $e->getMessage());}
         
        $req=$bdd->prepare('UPDATE articles
                        SET MAJ_article= :MAJ_article, CUPI_article= :CUPI_article, libelle= :libelle,
                        prix_unitaire= :prix_unitaire, duree_de_vie= :duree_de_vie, taux_entretien= :taux_entretien,
                        taux_remplacement= :taux_remplacement
                        WHERE id_article= :id_article');
        
        $req->execute(array(
            'id_article'=>$id_article,
            'MAJ_article'=>htmlentities($_POST['MAJ_article']),
            'CUPI_article'=>htmlentities($_POST['CUPI_article']),
            'libelle'=>htmlentities($_POST['libelle']),
            'prix_unitaire'=>htmlentities($_POST['prix_unitaire']),
            'duree_de_vie'=>htmlentities($_POST['duree_de_vie']),
            'taux_entretien'=>htmlentities($_POST['taux_entretien']),
            'taux_remplacement'=>htmlentities($_POST['taux_remplacement'])
            ));
        
        header('Location:maj.php?projet='.$id_projet.'&piece='.$id_piece.'&scenario='.$id_scenario.'');
?>

